//
//  showAlert.swift
//  CurrencyConverterApp
//
//  Created by Яков on 02.11.2021.
//

import UIKit

class AlertPopUpView {
    
    func showAlert(title: String? = nil, message: String? = nil, buttonTitle: String? = nil) -> UIAlertController {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: buttonTitle, style: .cancel, handler: nil)
        
        alertVC.addAction(okAction)
        return alertVC
    }
}
