//
//  ExchangeVC+Extensions.swift
//  CurrencyConverterApp
//
//  Created by Яков on 02.11.2021.
//

import Foundation
import UIKit

//MARK: - actions of text fields

extension ExchangeViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let validCharacters = NSCharacterSet(charactersIn:"0123456789").inverted

        let components = string.components(separatedBy: validCharacters)

        let characterFilter = components.joined(separator: "")

        if characterFilter == string {
            return true
        } else {
            if string == "." {
                let countDots = textField.text!.components(separatedBy:".").count - 1
                
                if countDots == 0 {
                    return true
                    
                } else {
                    if countDots > 0 && string == "." {
                        return false
                        
                    } else {
                        return true
                    }
                }
            } else {
                return false
            }
        }
    }
    
    func textFieldChecks() -> CheckingTextFields {
        
        if amountTextField.text == "" {
            return .amountIsEmpty
        }
        
        if amountTextField.text == "0" {
            return .amountZero
        }
        
        if fromCurrencyTextField.text == "" {
            return .fromCurrencyIsEmpty
        }
        
        if toCurrencyTextField.text == "" {
            return .toCurrencyIsEmpty
        }
        return .success
    }
    
}

//MARK: - building PickerView

extension ExchangeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listQuotes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = UIFont.systemFont(ofSize: CGFloat(20), weight: .semibold)
        label.textColor = .black
        label.textAlignment = .center
        label.text = listQuotes[row]
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if fromCurrencyTextField.isEditing {
            fromCurrencyTextField.text = listQuotes[row]
        } else if toCurrencyTextField.isEditing {
            toCurrencyTextField.text = listQuotes[row]
        }
        
        if self.textFieldChecks() == .success {
            self.showPriceOneFromCurrency(from: fromCurrencyTextField.text ?? "SGD",
                                            to: toCurrencyTextField.text ?? "SGD")
        }
        self.view.endEditing(true)
    }
    
}

//MARK: - funcs show the result of the conversion

extension ExchangeViewController {
    
    func loadingСurrency() {
        APIOperations().loadCurrencies() { currencies, error in
            
            guard error == nil else {
                self.present(AlertPopUpView().showAlert(title: "ERROR", message: error, buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let currencies = currencies else {
                self.present(AlertPopUpView().showAlert(title: "ERROR", message: "No data from response", buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            DispatchQueue.main.async {
                self.listQuotes = currencies
                self.currencyPickerView.reloadAllComponents()
            }
        }
    }
    
    func showPriceOneFromCurrency(from: String, to: String) {
        
        APIOperations().loadResult(from: from, to: to) { result, error in
            
            guard error == nil else {
                self.present(AlertPopUpView().showAlert(title: "ERROR", message: error, buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let result = result else {
                self.present(AlertPopUpView().showAlert(title: "ERROR", message: "No data from response", buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            self.sum = self.computation(amount: "1", ratio: result)
            
            DispatchQueue.main.async {
                self.costOfOneLabel.text = "1 \(from) = \(self.rounding(numeric: result, decimalPoint: 4)) \(to)"
            }
        }
    }

    func showGettingBackResult(from: String, to: String, amount: String) {
        
        APIOperations().loadResult(from: from, to: to) { result, error in
            
            guard error == nil else {
                self.present(AlertPopUpView().showAlert(title: "ERROR", message: error, buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            guard let result = result else {
                self.present(AlertPopUpView().showAlert(title: "ERROR", message: "No data from response", buttonTitle: "OK"), animated: true, completion: nil)
                return
            }
            
            self.sum = self.computation(amount: amount, ratio: result)
            
            RealmDatabase.shared.newRecording(from: from,
                                                to: to,
                                      costFromInTo: result,
                                            amount: amount,
                                          estimate: self.sum)
            
            DispatchQueue.main.async {
                self.resultLabel.text = "\(self.sum)"
                self.costOfOneLabel.text = "1 \(from) = \(self.rounding(numeric: result, decimalPoint: 4)) \(to)"
            }
        }
    }
    
}

//MARK: - funcs computation
extension ExchangeViewController {
    
    func computation(amount: String, ratio: Double) -> Double {
        var sum: Double = 0
        resultLabel.font = resultLabel.font.withSize(28)
        
        if let amount = Double(amount) {
            sum = amount * ratio
            
            if Int(sum).description.count >= 9 && Int(sum).description.count < 11 {
                resultLabel.font = resultLabel.font.withSize(24)
                
            } else if Int(sum).description.count >= 11 && Int(sum).description.count < 16 {
                    resultLabel.font = resultLabel.font.withSize(20)

            } else if Int(sum).description.count >= 16 {
                present(AlertPopUpView().showAlert(title: "Error",
                                                 message: "The calculation result has exceeded the permissible",
                                             buttonTitle: "OK"),
                        animated: true, completion: nil)
                amountTextField.text = ""
                return 0
            }
        }
        return rounding(numeric: sum, decimalPoint: 2)
    }
    
    func rounding(str: String? = nil, numeric: Double? = nil, decimalPoint: Int) -> Double {
        var number: Double = 1
        var point: Double = 1
        
        if let str = str {
            if let strNumber = Double(str) {
                number = strNumber
            }
        }
        
        if let numeric = numeric {
            number = numeric
        }
        
        var i = decimalPoint
        repeat {
            point *= 10
            i -= 1
        } while i > 0
        
        return round(number * point) / point
    }
    
}

//MARK: - new entry in history

//extension ExchangeViewController {
//
//    func newRecording(from: String, to: String, costFromInTo: Double, amount: String, estimate: Double) {
//
//        let generateID = Int.random(in: 100...1000) * Int.random(in: 10...100)
//        var conversion: OneConversion = OneConversion(operationID: 0,
//                                                 fromCurrency: "",
//                                                 toCurrency: "",
//                                                 costOneCurrencyFromInCurrencyTo: 0,
//                                                 amountCurrencyFrom: "",
//                                                 estimate: 0)
//        conversion.operationID = generateID
//        conversion.fromCurrency = from
//        conversion.toCurrency = to
//        conversion.costOneCurrencyFromInCurrencyTo = costFromInTo
//        conversion.amountCurrencyFrom = amount
//        conversion.estimate = estimate
//
//        conversionList.append(conversion)
//        conversionHistory.conversionList = self.conversionList
//
//        RealmDatabase.shared.realmDeleteAllObjects()
//        RealmDatabase.shared.saveData(conversionHistory)
//    }
//
//}
