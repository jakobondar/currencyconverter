//
//  ExchangeViewController.swift
//  CurrencyConverterApp
//
//  Created by Яков on 02.11.2021.
//

import UIKit

enum CheckingTextFields {
    case success
    case amountIsEmpty
    case amountZero
    case fromCurrencyIsEmpty
    case toCurrencyIsEmpty
}

class ExchangeViewController: UIViewController {
    
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var costOfOneLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet var fromCurrencyTextField: UITextField!
    @IBOutlet var toCurrencyTextField: UITextField!
    
    var currencyPickerView = UIPickerView()
    var listQuotes: [String] = []
    var sum: Double = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        amountTextField.delegate = self
        currencyPickerView.delegate = self
        currencyPickerView.dataSource = self
        fromCurrencyTextField.inputView = currencyPickerView
        toCurrencyTextField.inputView = currencyPickerView
        currencyPickerView.backgroundColor = .systemGray4
        
        //by gesture collapse all open views
        let tapper = UITapGestureRecognizer(target: self, action:#selector(endEditing))
        tapper.cancelsTouchesInView = true
        view.addGestureRecognizer(tapper)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadingСurrency()
    }
    
    @IBAction func reverseAction(_ sender: Any) {
        
        var tempData = ""
        tempData = fromCurrencyTextField.text ?? ""
        fromCurrencyTextField.text = toCurrencyTextField.text
        toCurrencyTextField.text = tempData
        showPriceOneFromCurrency(from: fromCurrencyTextField.text ?? "SGD",
                                   to: toCurrencyTextField.text ?? "SGD")
    }
    
    @IBAction func convertAction(_ sender: Any) {
        
        switch textFieldChecks() {
        case .amountZero:
            present(AlertPopUpView().showAlert(title: "Error, zero cannot be converted",
                                             message: "Change the value by one?",
                                         buttonTitle: "OK"), animated: true, completion: nil)
            amountTextField.text = "1"
            
        case .amountIsEmpty:
            present(AlertPopUpView().showAlert(title: "Error, insufficient data",
                                             message: "Indicate the amount of the original currency",
                                         buttonTitle: "OK"), animated: true, completion: nil)
            
        case .fromCurrencyIsEmpty:
            present(AlertPopUpView().showAlert(title: "Error, no currency selected",
                                             message: "Please select a currency to convert",
                                         buttonTitle: "OK"), animated: true, completion: nil)
            
        case .toCurrencyIsEmpty:
            present(AlertPopUpView().showAlert(title: "Error, no currency selected",
                                             message: "Please select a currency to convert",
                                         buttonTitle: "OK"), animated: true, completion: nil)
            
        default: showGettingBackResult(from: fromCurrencyTextField.text ?? "SGD",
                                         to: toCurrencyTextField.text ?? "SGD",
                                     amount: amountTextField.text ?? "1")
        }
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        let menuVC = MenuViewController(nibName: "MenuViewController", bundle: nil)
        menuVC.modalTransitionStyle = .crossDissolve
        present(menuVC, animated: true, completion: nil)
    }
    
    @objc func endEditing(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
}
