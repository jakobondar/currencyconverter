//
//  MenuVC+Extensions.swift
//  CurrencyConverterApp
//
//  Created by Яков on 07.11.2021.
//

import Foundation
import UIKit

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RealmDatabase.shared.databaseHistory.conversionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: myHistoryCellID, for: indexPath) as! MyHistoryTableViewCell
        cell.update(RealmDatabase.shared.databaseHistory.conversionList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            RealmDatabase.shared.databaseHistory.conversionList.remove(at: indexPath.row)
            RealmDatabase.shared.realmDeleteAllObjects()
            RealmDatabase.shared.saveData(RealmDatabase.shared.databaseHistory)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
}
