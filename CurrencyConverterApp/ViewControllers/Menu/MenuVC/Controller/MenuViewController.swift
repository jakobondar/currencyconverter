//
//  MenuViewController.swift
//  CurrencyConverterApp
//
//  Created by Яков on 07.11.2021.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var titleMenuLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var clearHistoryOutlet: UIButton!
    
    let myHistoryCellID = String(describing: MyHistoryTableViewCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: myHistoryCellID, bundle: nil), forCellReuseIdentifier: myHistoryCellID)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleMenuLabel.text = "Menu"
        tableView.isHidden = true
        clearHistoryOutlet.isHidden = true
        tableView.reloadData()
    }

    @IBAction func myHistoryAction(_ sender: Any) {
        
        if !RealmDatabase.shared.databaseHistory.conversionList.isEmpty {
            titleMenuLabel.text = "History"
            tableView.isHidden = false
            clearHistoryOutlet.isHidden = false
        } else {
            present(AlertPopUpView().showAlert(title: "Empty",
                                             message: "oops, no conversion records",
                                         buttonTitle: "OK"), animated: true, completion: nil)
        }
        
    }
    
    @IBAction func clearHistoryAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Clear the history",
                                    message: "Are you sure you want to erase all the currency transfer records?",
                             preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { action in
            
            RealmDatabase.shared.databaseHistory.conversionList.removeAll()
            RealmDatabase.shared.realmDeleteAllObjects()
            self.clearHistoryOutlet.isHidden = true
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        tableView.isHidden = true
        dismiss(animated: true, completion: nil)
    }
    
}
