//
//  MyHistoryTableViewCell.swift
//  CurrencyConverterApp
//
//  Created by Яков on 07.11.2021.
//

import UIKit

class MyHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var fromCurrencyLabel: UILabel!
    @IBOutlet weak var toCurrencyLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var priceOneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func update(_ data: OneConversionHistory) {
        fromCurrencyLabel.text = data.fromCurrency
        toCurrencyLabel.text = data.toCurrency
        amountLabel.text = data.amountCurrencyFrom
        resultLabel.text = floatToStr(data.estimate)
        priceOneLabel.text = "1 \(data.fromCurrency) = \(floatToStr(data.costOneCurrencyFromInCurrencyTo)) \(data.toCurrency)"
    }
    
    func floatToStr(_ numeric: Double) -> String {
        return String(numeric)
    }
    
}
