//
//  NetworkManager.swift
//  CurrencyConverterApp
//
//  Created by Яков on 02.11.2021.
//

import Foundation
import Alamofire
import UIKit
import RealmSwift

class NetworkManager {
    
    static let shared = NetworkManager()

    let apiKey = "b6d52306admsh2ac65f39c116260p1c3d5ejsnf1a1d40f92f0"
    let apiHost = "currency-exchange.p.rapidapi.com"
    
    func networkRequest(urlPath: String,
                         method: HTTPMethod,
                     parameters: [String: Any]? = nil,
                        headers: [String: String]? = nil,
              completionHandler: @escaping (Data?, String?) -> Void) {
        
        var staticHTTPHeaders: [String: String] = ["x-rapidapi-host": apiHost, "x-rapidapi-key": apiKey]
        
        var httpHeaders: [HTTPHeader] = []
        
        if let headers = headers {
            for (key, value) in headers {
                staticHTTPHeaders[key] = value
            }
        }
        
        staticHTTPHeaders.forEach { key, value in
            let httpHeader = HTTPHeader(name: key, value: value)
            httpHeaders.append(httpHeader)
        }
        
        AF.request(urlPath,
                   method: method,
               parameters: parameters,
                  headers: HTTPHeaders(httpHeaders)).response { response in
            
            switch response.result {
            case .success(let data):
                completionHandler(data, nil)
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
}
