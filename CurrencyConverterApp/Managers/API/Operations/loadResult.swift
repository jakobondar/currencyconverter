//
//  loadResult.swift
//  CurrencyConverterApp
//
//  Created by Яков on 02.11.2021.
//

import Foundation

class APIOperations {
    
    func loadCurrencies(completionHandler: @escaping ([String]?, String?) -> Void) {
        
        NetworkManager.shared.networkRequest(urlPath: URLs.currenciesURL, method: .get) { data, error in
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode([String].self, from: data)
                completionHandler(response, nil)
            
            } catch let errorDecode as NSError {
                completionHandler(nil, errorDecode.localizedDescription)
            }
        }
    }
    
    func loadResult(from: String, to: String, completionHandler: @escaping (Double?, String?) -> Void) {
        
        let parameters: [String: Any] = ["from": from, "to": to]
        
        NetworkManager.shared.networkRequest(urlPath: URLs.exchangeURL, method: .get, parameters: parameters) { data, error in
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            guard let data = data else {
                completionHandler(nil, "Unknown error")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(Double.self, from: data)
                completionHandler(response, nil)
            
            } catch let errorDecode as NSError {
                completionHandler(nil, errorDecode.localizedDescription)
            }
        }
    }
    
}
