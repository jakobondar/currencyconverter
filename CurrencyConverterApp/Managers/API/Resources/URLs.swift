//
//  URLs.swift
//  CurrencyConverterApp
//
//  Created by Яков on 02.11.2021.
//

import Foundation

struct URLs {
    static let currenciesURL = "https://currency-exchange.p.rapidapi.com/listquotes"
    static let exchangeURL = "https://currency-exchange.p.rapidapi.com/exchange"
//    let urll = "https://rapidapi.com/natkapral/api/currency-converter5/"
}
