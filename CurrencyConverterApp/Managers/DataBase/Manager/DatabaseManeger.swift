//
//  DatabaseManeger.swift
//  CurrencyConverterApp
//
//  Created by Яков on 06.11.2021.
//

import Foundation
import RealmSwift

class RealmDatabase {

    static let shared = RealmDatabase()
    private let realm: Realm = try! Realm()
    
    var databaseHistory = ConversionHistory(conversionList: [])
    
    private init() {
        if let object = getData() {
            databaseHistory = object
        }
    }
    
    func saveData(_ data: ConversionHistory) {
        let object = transformData(data)
        try! realm.write {
            realm.add(object)
        }
    }

    func getData() -> ConversionHistory? {
        guard let data = realm.objects(ConversionsDatabase.self).first else { return nil }
        return readRealmData(data)
    }
    
//MARK: - clear realm data
    
    func realmDeleteAllObjects() {
        do {
            let realm = try Realm()

            let objects = realm.objects(ConversionsDatabase.self)

            try! realm.write {
                realm.delete(objects)
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }
    }
    
//MARK: - transform to Realm Model
    
    private func transformData(_ data: ConversionHistory) -> ConversionsDatabase {
        let object: ConversionsDatabase = ConversionsDatabase()
        let conversionList = List<OneConversionDatabase>()
        
        for item in data.conversionList {
            let conversion: OneConversionDatabase = OneConversionDatabase()
            conversion.operationID = item.operationID
            conversion.fromCurrency = item.fromCurrency
            conversion.toCurrency = item.toCurrency
            conversion.costOneCurrencyFromInCurrencyTo = item.costOneCurrencyFromInCurrencyTo
            conversion.amountCurrencyFrom = item.amountCurrencyFrom
            conversion.estimate = item.estimate
            
            conversionList.append(conversion)
        }
        object.conversionList = conversionList
        return object
    }
    
//MARK: - transform back to our Model
    
    private func readRealmData(_ data: ConversionsDatabase) -> ConversionHistory {
        var conversionList = [OneConversionHistory]()
        var object: ConversionHistory = ConversionHistory(conversionList: conversionList)
        
        for item in data.conversionList {
            var conversion: OneConversionHistory = OneConversionHistory(operationID: 0,
                                                          fromCurrency: "",
                                                          toCurrency: "",
                                                          costOneCurrencyFromInCurrencyTo: 0,
                                                          amountCurrencyFrom: "",
                                                          estimate: 0)
            conversion.operationID = item.operationID
            conversion.fromCurrency = item.fromCurrency
            conversion.toCurrency = item.toCurrency
            conversion.costOneCurrencyFromInCurrencyTo = item.costOneCurrencyFromInCurrencyTo
            conversion.amountCurrencyFrom = item.amountCurrencyFrom
            conversion.estimate = item.estimate
            
            conversionList.append(conversion)
        }
        object.conversionList = conversionList
        return object
    }
    
//MARK: - new entry in history
    
    func newRecording(from: String, to: String, costFromInTo: Double, amount: String, estimate: Double) {
    
        let generateID = Int.random(in: 100...1000) * Int.random(in: 10...100)
        var conversion: OneConversionHistory = OneConversionHistory(operationID: 0,
                                                 fromCurrency: "",
                                                 toCurrency: "",
                                                 costOneCurrencyFromInCurrencyTo: 0,
                                                 amountCurrencyFrom: "",
                                                 estimate: 0)
        conversion.operationID = generateID
        conversion.fromCurrency = from
        conversion.toCurrency = to
        conversion.costOneCurrencyFromInCurrencyTo = costFromInTo
        conversion.amountCurrencyFrom = amount
        conversion.estimate = estimate
        
        self.databaseHistory.conversionList.append(conversion)

        RealmDatabase.shared.realmDeleteAllObjects()
        RealmDatabase.shared.saveData(self.databaseHistory)
    }
    
}

