//
//  ConversionHistoryModel.swift
//  CurrencyConverterApp
//
//  Created by Яков on 06.11.2021.
//

import Foundation

struct ConversionHistory {
    dynamic var conversionList: [OneConversionHistory]
}

struct OneConversionHistory {
    dynamic var operationID: Int
    dynamic var fromCurrency: String
    dynamic var toCurrency: String
    dynamic var costOneCurrencyFromInCurrencyTo: Double
    dynamic var amountCurrencyFrom: String
    dynamic var estimate: Double
}
