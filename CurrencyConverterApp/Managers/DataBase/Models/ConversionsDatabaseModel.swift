//
//  ConversionsDatabase.swift
//  CurrencyConverterApp
//
//  Created by Яков on 06.11.2021.
//

import Foundation
import RealmSwift

class ConversionsDatabase: Object {
    var conversionList = List<OneConversionDatabase>()
}

class OneConversionDatabase: Object {
    @objc dynamic var operationID: Int = 0
    @objc dynamic var fromCurrency: String = ""
    @objc dynamic var toCurrency: String = ""
    @objc dynamic var costOneCurrencyFromInCurrencyTo: Double = 0
    @objc dynamic var amountCurrencyFrom: String = ""
    @objc dynamic var estimate: Double = 0
}
